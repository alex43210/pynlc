from setuptools import setup, find_packages


readme = 'Keras CNN-based NLC'
setup(
    name='pynlc',
    version='0.1',
    description=readme,
    author='Alexander Pozharskii',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
    ],
    packages=find_packages(exclude='demo'),
    install_requires=['keras', 'gensim', 'nltk', 'numpy', 'python-Levenshtein']
)
